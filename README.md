# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Simple old angular 2 app with backend auth as express app and apis for countries and universities
Using node: v6.10.2

### How do I get set up? ###

1. Install and setup mongoDB
2. Clone and run repo https://bitbucket.org/danielvp/auth_express
3. Clone and run repo https://bitbucket.org/danielvp/rest_countries
4. Clone this repo and run npm install and npm start
5. Enjoy


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
