import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { BlockGuard } from './shared/block-guard.service';

import { PageNotFoundComponent } from './page-not-found.component';

export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomeModule' },
  { path: 'login', loadChildren:'./login/login.module#LoginModule', canLoad: [BlockGuard] },
  { path: 'signup', loadChildren:'./signup/signup.module#SignUpModule', canLoad: [BlockGuard] },
  { path: 'logout', loadChildren:'./logout/logout.module#LogoutModule' },
  { path: 'universities', loadChildren:'./universities/universities.module#UniversitiesModule', canLoad: [BlockGuard] },
  { path: 'countries', loadChildren:'./countries/countries.module#CountriesModule', canLoad: [BlockGuard] },
  { path: '**', component: PageNotFoundComponent }
];
@NgModule({
    imports:[RouterModule.forRoot(rootRouterConfig)],
    providers:[BlockGuard],
    exports: [ RouterModule ]
})
export class AppRoutingModule{}
