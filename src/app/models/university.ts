import { Country } from './country';
export class University{
    webPage?:string;
    country?: Country;
    domain?: string;
    name?:string;
}
