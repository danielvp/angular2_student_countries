import { Genre } from './../shared/utills'; 

export interface IUser{
    email: string;
}

export class User implements IUser{
    firstName: string;
    lastName: string;
    genre: Genre;
    email: string;
    password: string;
    passwordConfirm: string;
    country: string;
    university: string;
}

export class MinimalUser implements IUser{
    email: string;
    firstName: string;
    country: string;
    university: string;
    lastName: string;
}
