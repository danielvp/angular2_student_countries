import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule } from '@angular/forms';
import {CountriesComponent} from './countries.component';
import { CountriesService } from './countries.service';
import { CountriesRouting } from './countries-routing.module';
import { CountryDetailsComponent } from './country-details.component';
import {Ng2PaginationModule} from 'ng2-pagination'; // <-- import the module
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

@NgModule({
    imports: [CommonModule, FormsModule,ReactiveFormsModule, CountriesRouting, Ng2PaginationModule, Ng2AutoCompleteModule],
    declarations: [CountriesComponent],
    providers: [CountriesService]
})
export class CountriesModule{}
