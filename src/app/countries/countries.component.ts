import { Component } from '@angular/core';
import { CountriesService} from './countries.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';

@Component({
    selector:'countries',
    templateUrl: 'countries.component.html'
})
export class CountriesComponent{

    perPage = 9;
    searchCountriesInput: HTMLInputElement;
    countriesArray: Observable<any>;
    total : number;
    constructor(public countriesService: CountriesService,private route: ActivatedRoute,
                private router: Router,  private _sanitizer: DomSanitizer, private builder: FormBuilder){
                }

                p: number = 1;
                loading: boolean;

                public countriesForm: FormGroup;

                ngOnInit(){
                    this.countriesForm = this.builder.group({
                        searchedTerm: "",
                    })
                    this.route
                    .queryParams
                    .subscribe(params => {
                        if(params['page'])
                            this.getPage(+params['page']);
                        else 
                            this.getPage(1);
                    });

                    this.countriesForm.valueChanges
                    .debounceTime(400)
                    .pluck("searchedTerm")
                    .distinctUntilChanged()
                    .switchMap((searchedTerm:any)=>{
                        if(searchedTerm!==""){
                            return this.countriesService.getCountries(searchedTerm);
                        }
                        return this.countriesService.getByRange(0,9);
                    }).subscribe((resp)=>{
                        setTimeout(()=>{
                            if(this.searchCountriesInput){
                                this.searchCountriesInput.blur();
                                this.searchCountriesInput.focus();
                            }
                        },100);

                        if(resp.length){
                            this.total = resp.length;
                            this.countriesArray = Observable.of(resp);
                        }else
                            {
                                this.total = resp.size;
                                this.p = 1;
                                this.loading = false;
                                this.countriesArray = Observable.of(resp.countries);

                            }

                    });
                }
                getPage(page: number){
                    this.loading = true;
                    this.router.navigate(['/countries'], { queryParams: { page:  page } }); 
                    const start = (page - 1) * this.perPage;
                    const end = start + this.perPage;
                    if(this.countriesForm.value.searchedTerm===""){
                        this.countriesArray = this.countriesService.getByRange(start,end)
                        .do(res => {
                            this.total = res.size;
                            this.p = page;
                            this.loading = false;
                        })

                        .map(item=>
                             {
                                 return item.countries;

                             });
                    }else
                        {
                            this.countriesArray = this.countriesService.getCountries(this.countriesForm.value.searchedTerm).map(
                                (resp)=>{
                                    setTimeout(()=>{ 
                                        if(this.searchCountriesInput && this.total>1)
                                            this.searchCountriesInput.focus();
                                    },100);

                                    this.total = resp.length;
                                    this.p = page;
                                    if(resp.length > 9)
                                        return resp.slice(start,end);
                                    else return resp;

                                });
                        }
                }
                countryChanged(val: any)
                {
                    if(val){
                        this.countriesForm.setValue({searchedTerm: val.name.common});
                    }
                }
                onFocus(inputCountries: HTMLInputElement ){
                    if(!this.searchCountriesInput){
                        this.searchCountriesInput = inputCountries;
                    }
                }

                autocompleListFormatter = (data: any) : SafeHtml => {
                    let html = `<span>${data.name.common}</span>`;
                    return this._sanitizer.bypassSecurityTrustHtml(html);
                }
                onSubmit(model: any, isValid: boolean){
                }
                nextPage(){
                }
}
