import {Injectable } from '@angular/core';
import {REST} from './../shared/utills'; 
import { URLSearchParams, Http, Response } from '@angular/http';
import { HttpUtills } from './../shared/utills'; 
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CountriesService extends HttpUtills{


    constructor(public http: Http){
       super(); 
    }

    getByRange(lowerLimit: number, upperLimit: number): Observable<any>{
        let httpOptions = new URLSearchParams(); 
        return this.http.get(`${REST.COUNTRIES}/v1/range/${lowerLimit}/${upperLimit}`, this.getJsonOptions() )
                        .map(this.decodeJSON);
    }
    
    getCountries(country: string): Observable<any>{
        return this.http.get(`${REST.COUNTRIES}/v1/country/search/${country}`, this.getJsonOptions() )
                        .map(this.decodeJSON);
    }
    
    decodeJSON(res: Response){
        let body = res.json();
        return body;
    }
}
