import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {REST} from './../shared/utills'; 
import { URLSearchParams, Http, Response, Headers } from '@angular/http';
import { Student } from './../models/student';
import { HttpUtills } from './../shared/utills'; 
@Injectable()
export class StudentService extends HttpUtills{

    constructor(private http: Http){
        super();
    }
    getStudentsByUniversity(str: string) : Observable<any>{
        let search = new URLSearchParams(); 
        search.set('university',str);
        if(localStorage.getItem('token'))
        search.set('access_token', localStorage.getItem('token').slice(4,localStorage.getItem('token').length));
        return this.http.get(`${REST.AUTH}/admin/student/list/by/university`, {search})
        .map(this.decodeJSON);

    }

    addStudent(student: Student): Observable<any>{
        let headers_ = new Headers();
        headers_.append('Content-Type', 'application/json');

        return this.http.post(`${REST.AUTH}/admin/student`,
            {
                student,
                access_token: localStorage.getItem('token') ? localStorage.getItem('token').slice(4,localStorage.getItem('token').length) : 0
            }, {headers: headers_})
        .map(this.decodeJSON);
    }

    deleteStudent(student: Student): Observable<any>{
        let search = new URLSearchParams(); 
        search.set('nickName',student.nickName);
        if(localStorage.getItem('token'))
        search.set('access_token', localStorage.getItem('token').slice(4,localStorage.getItem('token').length));
        return this.http.delete(`${REST.AUTH}/admin/student`, {search})
        .map(this.decodeJSON);
    }

    updateStudent(student: Student): Observable<any>{
        let headers_ = new Headers();
        headers_.append('Content-Type', 'application/json');

        return this.http.put(`${REST.AUTH}/admin/student`,
            {
                student,
                access_token: localStorage.getItem('token') ? localStorage.getItem('token').slice(4,localStorage.getItem('token').length) : 0
            }, {headers: headers_})
        .map(this.decodeJSON);
    }

    updateUniversityByStudent(student: Student): Observable<any>{
        let headers_ = new Headers();
        headers_.append('Content-Type', 'application/json');

        return this.http.put(`${REST.AUTH}/admin/student/university`,
            {
                student,
                access_token: localStorage.getItem('token') ? localStorage.getItem('token').slice(4,localStorage.getItem('token').length) : 0
            }, {headers: headers_})
        .map(this.decodeJSON);
    }

    decodeJSON(res: Response){
        let body = res.json();
        return body;
    }

}
