import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsListComponent } from './students-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentService } from './student.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, Ng2SmartTableModule], 
    declarations: [StudentsListComponent],
    exports: [StudentsListComponent],
    providers: [ StudentService ]

})
export class StudentModule{}

