import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { StudentService } from './student.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { LocalDataSource} from 'ng2-smart-table'

import { University} from './../models/university';

@Component({
    selector: 'students-list',
    templateUrl: 'students-list.component.html'
})
export class StudentsListComponent implements OnInit{

    @Output()
    usersLoaded : EventEmitter<any> = new EventEmitter<any>();

    @Output()
    userRowSelectedChanged: EventEmitter<any> = new EventEmitter<any>();

    @Output()
    selectedUniversityChanged : EventEmitter<University> = new EventEmitter<University>();

    _selectedUniversity: University;
    get selectedUniversity():  University{
        return this._selectedUniversity;
    }

    @Input('selectedUniversity')
    set selectedUniversity(str:  University)
    {  
        if(str){
            this._selectedUniversity = str;
            this.selectedUniversityChanged.emit(this._selectedUniversity);
        }
    }
    settings = {
        columns: {
            nickName: {
                title: 'Nickname'
            },
            firstName: {
                title: 'First name'
            },
            lastName: {
                title: 'Last name'
            }
        },
        add: {
            confirmCreate: true,
        },
        edit: {
            confirmSave: true,
        },
        delete: {
            deleteButtonContent: 'Delete',
            confirmDelete: true,
        },
    };
    source: LocalDataSource;

    constructor(private studentService : StudentService, private router: Router){
        this.source = new LocalDataSource();
        this.selectedUniversityChanged.subscribe(
            str=>
            {
                if(str){
                    this.studentService.getStudentsByUniversity(str.name).subscribe(sl=>{
                        this.source.load(sl.universities);
                        this.usersLoaded.emit(sl.universities); 
                    });
                }
            });
    }

    ngOnInit(){
    }

    userRowSelected(ev : any){
        this.userRowSelectedChanged.emit(ev); 
    }

    editConfirmation(ev : any){
        if(ev){
            this.studentService.updateStudent(ev.newData).subscribe(resp=>{
                if(resp.error){
                    alert('there was an error');
                }else
                    {
                        alert('Successful edited');
                        ev.confirm.resolve();
                    }
            });

        }
    }
    confirmConfirmation(ev : any){
        this.source.getAll().then(el=>{
            if(el.find(elx=>elx.nickName === ev.newData['nickName'])){
                alert("Element exists");
            }
            else
                {
                    ev.newData.universities = [{ country: this.selectedUniversity.country,name: this.selectedUniversity.name }];
                    this.studentService.addStudent(ev.newData).subscribe(resp=>{
                        if(resp.error){
                            alert("there was an error");
                        }else
                            {
                                alert("Successful stored");
                                ev.confirm.resolve();
                            }
                    });
                }
        });
    }

    deleteConfirmation(event : any){
        if(event)
            {
                this.studentService.deleteStudent(event.data).subscribe(resp=>{
                    if(resp.error){
                        alert("there was an error");
                    }else
                        {
                            alert("Successful stored");
                            event.confirm.resolve();
                        }

                });
            }
    }
}
