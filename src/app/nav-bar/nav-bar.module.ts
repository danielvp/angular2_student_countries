import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { NavBarComponent } from './nav-bar.component';
import { MaterialModule } from '@angular/material';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './../app-routing.module';
@NgModule({
    imports: [CommonModule, AppRoutingModule,  MaterialModule ],
    declarations: [NavBarComponent],
    exports: [NavBarComponent]

})
export class NavBarModule{}
