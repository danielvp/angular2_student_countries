import { Component , OnInit} from '@angular/core';
import { AuthService } from './../shared/auth.service';

@Component({
    selector:'nav-bar',
    templateUrl: 'nav-bar.component.html'
})
export class NavBarComponent implements OnInit{

    country_class: string;
    constructor(public authService : AuthService){
    }

    ngOnInit(){
        this.authService.authenticateState$.subscribe((loggedIn)=>{
            this.country_class=loggedIn ? `flag-icon-${this.authService.loggedInUser.country.toLowerCase()}` : "";
        
        });
    }
    
}
