import { NgModule } from '@angular/core'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { LoginModule} from './login/login.module';
import { HomeModule } from './home/home.module';
import { PageNotFoundComponent } from './page-not-found.component';
import { SignUpModule } from './signup/signup.module';
import { NavBarModule } from './nav-bar/nav-bar.module';
import { AuthService } from './shared/auth.service';
import { UniversitiesService } from './shared/universities.service';
import { LogoutModule } from './logout/logout.module';
import { UniversitiesModule } from './universities/universities.module';
import { CountriesModule } from './countries/countries.module';
import { StudentModule } from './student/student.module';

import 'hammerjs';
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    LoginModule,
    HomeModule,
    SignUpModule,
    NavBarModule,
    LogoutModule,
    UniversitiesModule,
    CountriesModule,
    StudentModule
  ],
  providers:[AuthService, UniversitiesService],
  bootstrap: [ AppComponent ]
})
export class AppModule {

}
