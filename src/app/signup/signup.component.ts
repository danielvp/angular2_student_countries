import {Component, OnInit, ElementRef, Renderer} from '@angular/core';
import { User } from './../models/user';
import { Genre } from './../shared/utills';
import { AuthService } from './../shared/auth.service';
import { UniversitiesService } from './../shared/universities.service';
import { AuthError } from './../shared/utills';
import { Observable } from 'rxjs/Observable';
import { FormControl } from '@angular/forms';

declare var jQuery:any;

@Component({
    selector:'signup',
    templateUrl: 'signup.component.html'
})
export class SignUpComponent implements OnInit{
    searchedTerm: FormControl = new FormControl();
    user : User;
    searchUniversitiesInput: HTMLInputElement;
    genre : any;
    authError: AuthError;
    universitiesArray: Observable<Array<string>>;
    country_codes: Observable<any>;


    constructor(private renderer: Renderer, private elementRef: ElementRef,
        public authService : AuthService, public universitiesService: UniversitiesService ){
        this.genre = Genre;
        // this.universitiesArray = Observable.from(['type something']);
        this.country_codes = this.universitiesService.getCountryCodes();
    }
    
    onFocus(inputUniversities: HTMLInputElement ){
        if(!this.searchUniversitiesInput){
            this.searchUniversitiesInput = inputUniversities;
        }
    }
    universityChanged(val: string){
        if(val && this.user)
        {
            this.user.university=val;
        }
    }
    
    ngOnInit(){
        this.user = new User;
        this.universitiesArray = this.searchedTerm.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap((searchedTerm) =>{
                return this.universitiesService.searchUniversities(searchedTerm);
            }).map((el)=>{
             setTimeout(()=>{
                 if(this.searchUniversitiesInput)
                    this.searchUniversitiesInput.blur();
                if(this.searchUniversitiesInput)
                 this.searchUniversitiesInput.focus();
             },100);
            return el;
        });
        this.searchedTerm.setValue("",{ emitEvent: true });//{onlySelf: true, emitEvent: true});
    }
    onSubmit(model: User, isValid: boolean){
        model['university']=this.user.university;
    
        this.authError = undefined;
        this.authService.signUp(model)
            .subscribe(res => {
                this.authError = res;
                if(res.success){
                }else{
                }
            });
    }
}
