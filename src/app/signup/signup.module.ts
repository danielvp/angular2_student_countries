import {NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SignUpComponent} from './signup.component';
import {SignUpRoutingModule} from './signup-routing.module';
import { SharedModule } from './../shared/shared.module';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
@NgModule({
 imports:[CommonModule, FormsModule, ReactiveFormsModule, SignUpRoutingModule, SharedModule, Ng2AutoCompleteModule],
 declarations: [ SignUpComponent]
})
export class SignUpModule{
}
