import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { User } from './../models/user';
import { Genre } from './../shared/utills';
import { AuthService } from './../shared/auth.service';
import { AuthError } from './../shared/utills';
@Component({
    selector:'',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit{
    user : User; 
    genre : any;
    authError: AuthError; 
    constructor(private router: Router, public authService : AuthService ){
        this.genre = Genre;
    }
    ngOnInit(){
        this.user = new User;
    }
    onSubmit(model: User, isValid: boolean){
        this.authError = undefined;
        this.authService.signIn(model)
            .subscribe(res => {
                this.authError = res;
                if(res.success){
                    this.router.navigate(['/universities']);
                }else{
                }
            });
    }
}
