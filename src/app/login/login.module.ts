import {NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login.component';
import {LoginRoutingModule} from './login-routing.module';
import { SharedModule } from './../shared/shared.module';
  
@NgModule({
 imports:[CommonModule, FormsModule, LoginRoutingModule, SharedModule],
 declarations: [ LoginComponent]
})
export class LoginModule{
}
