import {NgModule } from '@angular/core';
import {CommonModule } from '@angular/common';
import { UniversitiesComponent } from './universities.component';
import { UniversitiesRoutingModule } from './universities-routing.module';
import { MaterialModule } from '@angular/material';
import { UniversitiesList } from './universities-list.component';
import { AdminUniversitiesComponent } from './admin-universities.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2CompleterModule } from "ng2-completer";
import { StudentModule } from './../student/student.module';
import { DragulaModule } from 'ng2-dragula';
@NgModule({
    imports: [CommonModule, Ng2CompleterModule, FormsModule, ReactiveFormsModule, UniversitiesRoutingModule, MaterialModule, StudentModule, DragulaModule],
    declarations: [UniversitiesComponent, UniversitiesList, AdminUniversitiesComponent]
})
export class UniversitiesModule{}
