import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
@Component({
    selector: 'universities',
    templateUrl: 'universities.component.html'
})
export class UniversitiesComponent implements AfterViewInit, OnInit {
    
    routeEvents : Subscription ;

    constructor( private route: ActivatedRoute, private router: Router)
    {
    }

    ngOnInit(){
        this.routeEvents = this.router.events
            .subscribe((rout) => {
                if(rout['url'] && rout['url'].indexOf('admin') > -1)
                {
                    this.activeLinkIndex = 1;        
                }else
                {
                    this.activeLinkIndex = 0;
                }
            });
    }
    public ngAfterViewInit() {
        this.activeLinkIndex =
            this.tabLinks.indexOf(this.tabLinks.find(tab =>this.router.url.indexOf(tab.link) != -1));
    }
    ngOnDestroy(){
        this.routeEvents.unsubscribe();
    }
    tabLinks = [
        {label: 'Universities', link: '/universities'},
        {label: 'Admin', link: '/universities/admin'}
    ];

    activeLinkIndex = 0;
}
