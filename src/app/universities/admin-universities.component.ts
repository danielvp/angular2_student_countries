import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DragulaService } from 'ng2-dragula';
import { UniversitiesService } from './../shared/universities.service';
import { Observable } from 'rxjs/Observable';
import { ArrayUtills } from './../shared/utills';
import { FormGroup, FormControl, FormBuilder, FormArray,Validators } from '@angular/forms';
import { StudentService } from './../student/student.service';
import { University} from './../models/university';
@Component({
    selector: 'admin-universities',
    templateUrl: 'admin-universities.component.html',
})
export class AdminUniversitiesComponent{

    universities : Array<any> = [{name: 'none'}];

    public adminForm: FormGroup;
    nickName : string;
    universities_added : Array<any>= [{name:'none'}];
    selectedUniversity: University;
    constructor(private studentService : StudentService,
                private route: ActivatedRoute,
                private universitiesService : UniversitiesService, 
                private dragulaService:DragulaService,  
                private builder: FormBuilder) {
                    if(!dragulaService.find('universities-bag')){
                        dragulaService.setOptions('universities-bag', {
                            copy: true,
                            removeOnSpill: true
                        });
                    }
                    dragulaService.out.subscribe((value) => {
                        this.onOut(value.slice(1));
                    });
                    dragulaService.dropModel.subscribe((value:any) => {
                        this.onDropModel(value.slice(1));
                    });
                }

                selectedRowChanged(ev: any){
                    this.nickName = ev.data.nickName;
                    this.universities_added = ev.data.universities ? ev.data.universities : [this.selectedUniversity]  ;
                }
                private onDropModel(args:any):void {
                    let [el, target, source] = args;
                    if( source.dataset && +source.dataset.users!==0)
                        {

                            this.universities = this.universities.filter(univ=> univ.name.indexOf(el.innerHTML) === -1);
                            this.universities = ArrayUtills.uniq(this.universities, (a,b)=> a.name.localeCompare(b.name));
                        }else
                            {
                                this.universities_added.push(this.universities.find(uni=>uni.name==el.innerHTML));
                                this.universities_added = ArrayUtills.uniq(this.universities_added, (a,b)=> a.name.localeCompare(b.name));

                            }
                }

                private onOut(args) {
                    let [e, el, container] = args;
                    if(container && +container.dataset.users===1 && e.innerHTML.indexOf("none")===-1){
                        this.universities_added.splice(this.universities_added.findIndex((element, index, array)=>{
                            return element.name.indexOf(e.innerHTML)!==-1 ;
                        }),1);

                        if(this.universities_added.length === 0)
                            {
                                this.universities_added.push({name: 'none'});
                            }
                    }
                }

                ngOnInit(){
                    this.adminForm = this.builder.group({
                        searchedUniversity: "",
                    })

                    this.adminForm.valueChanges
                    .pluck('searchedUniversity')
                    .distinct()
                    .debounceTime(400)
                    .switchMap((term: any) =>  this.universitiesService.searchUniversities(term, true))
                    .subscribe(el=>{
                        this.universities = el;
                    });

                    this.universitiesService
                    .searchUniversities(null, true)
                    .subscribe(el=>{
                        this.universities = el;
                    });

                    this.route.queryParams.subscribe(el=>{
                        if(el)
                            {
                                this.selectedUniversity = {name : el.title, country: el.country};
                            }
                    });
                }

                submitChanges()
                {
                    this.studentService.updateUniversityByStudent({nickName: this.nickName, universities:this.universities_added})
                    .subscribe(el=>{

                    });
                }
                loadUsers(users : any){

                }
}
