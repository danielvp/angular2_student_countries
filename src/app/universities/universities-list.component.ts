import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CompleterService, CompleterItem, CompleterData, RemoteData } from 'ng2-completer';
import { UniversitiesService } from './../shared/universities.service';
import { Observable } from 'rxjs/Observable';
import { REST } from './../shared/utills';
@Component({
    selector: 'universities-list',
    templateUrl: 'universities-list.component.html' 
})
export class UniversitiesList implements OnInit{
    protected selectedUniversity: CompleterItem;
    universitiesArray: Observable<Array<string>>;
    private dataRemote: RemoteData;

    constructor(private router: Router, private universitiesService : UniversitiesService, private completerService: CompleterService){
        this.dataRemote = completerService.remote(
            null,
            null,
            "name");
        this.dataRemote.urlFormater(term => {
            return `${REST.UNIVERSITY}/search?name=${term}&limit=10`;
        });
    }

    ngOnInit(){

    }

    onSelected(item: CompleterItem) {
        if(item){
            this.selectedUniversity = item;
            this.router.navigate(['/universities/admin'], { queryParams:{title: item.title, country: item.originalObject.country } }  );
        }
    }
}
