import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UniversitiesComponent } from './universities.component';
import { UniversitiesList } from './universities-list.component';
import {  AdminUniversitiesComponent } from './admin-universities.component';

const routes : Routes = [
    {
        path: '', component: UniversitiesComponent, 
        children: [
            {
                path: '',
                children:[
                    { path: '', component: UniversitiesList },
                    { path: 'admin',  component: AdminUniversitiesComponent } 
                ]
            }
        ]
    
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class UniversitiesRoutingModule{
}
