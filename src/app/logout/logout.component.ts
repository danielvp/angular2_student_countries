import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../shared/auth.service';
@Component({
    selector:'logout',
    template: ''
})
export class LogoutComponent{
    constructor(private authService: AuthService, private router: Router){
        if(this.authService.isLoggedIn){
            this.authService.logout();
            this.router.navigate(['/login']);
        }
    }
}
