import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from './logout.component';

export const routes : Routes = [
    { path: '', component: LogoutComponent}
];
@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LogoutRoutingModule{

}
