import { RequestOptions, Headers } from '@angular/http';

export class REST{
    static readonly AUTH: string = "http://139.59.146.61:3500/api";
    static readonly COUNTRIES: string = "http://139.59.146.61:3100/api";
    static readonly UNIVERSITY: string = "http://139.59.146.61:5000/universities";
}

export enum Genre{
    Male,
        Female
}
export interface AuthError{
    success: boolean;
    message: string;
}

export class ArrayUtills{

    static uniq(a,callBack) {
        if(!callBack){
            return a.sort().filter(function(item, pos, ary) {
                 return !pos || item != ary[pos - 1];
            })
        }
        else{
            
            return a.sort(callBack).filter(function(item, pos, ary) {
                 return !pos || item.name != ary[pos - 1].name;
            })
            
        }
    }
}

export class HttpUtills{

    getJsonOptions(): RequestOptions{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }


}
