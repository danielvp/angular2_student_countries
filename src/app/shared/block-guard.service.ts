import { Injectable }       from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanActivateChild,
    NavigationExtras,
    CanLoad, Route
}                           from '@angular/router';
import { AuthService }      from './auth.service';

@Injectable()
export class BlockGuard implements CanActivate, CanActivateChild, CanLoad {
    
    allowed_routes : String[] = ['/login','/logout','/signup','/home'];
    restricted_routes : String[] = ['/universities','/countries'];

    constructor(private authService: AuthService, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;

        return this.checkLogin(url);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    canLoad(route: Route): boolean {
        let url = `/${route.path}`;

        return this.checkLogin(url);
    }
    
    restrictedRoutes(url: string): boolean{
        return this.restricted_routes.find(route => route.startsWith(url)) && this.authService.isLoggedIn;
    }
    allowRoutes(url: string): boolean{
        return this.allowed_routes.find((allowedUrl)=>allowedUrl.startsWith(url)) && !this.authService.isLoggedIn;
    }
    checkLogin(url: string): boolean {
        if(this.restrictedRoutes(url)){
            return true;
            
        }else
        if (this.allowRoutes(url)) {
            return true;
        }

        this.authService.redirectUrl = url;

        let sessionId = 123456789;

        let navigationExtras: NavigationExtras = {
            queryParams: { 'session_id': sessionId },
            fragment: 'anchor'
        };

        this.router.navigate(['home'], navigationExtras);
        return false;
    }
}
