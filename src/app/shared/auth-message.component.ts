import { Component, Input } from '@angular/core';
import { AuthError } from './utills';

@Component({
    selector: 'auth-message',
    templateUrl: 'auth-message.component.html'
})
export class AuthMessageComponent{
    @Input()
    authError: AuthError;
}

