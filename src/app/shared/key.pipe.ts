import { Component, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'keys'
})
export class KeyPipe implements PipeTransform{

    transform(value : any, args: string[] ) : any{
        let keys = [];
        for(let valueOfEnum in value){
            let isValueANumber = parseInt(valueOfEnum) >= 0;
            if(isValueANumber){
                keys.push({key: valueOfEnum, data: value[valueOfEnum]});
            }
        }
        return keys;
    }
}
