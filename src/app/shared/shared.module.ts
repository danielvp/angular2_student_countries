import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeyPipe } from './key.pipe';
import { AuthService } from './auth.service';
import { HttpModule} from '@angular/http';
import { AuthMessageComponent } from './auth-message.component';
import { EqualValidatorDirective } from './equal-validator.directive';

@NgModule({
    imports:[ CommonModule, HttpModule],
    declarations: [KeyPipe, AuthMessageComponent, EqualValidatorDirective],
    exports: [ KeyPipe , AuthMessageComponent, EqualValidatorDirective]
})
export class SharedModule{}

