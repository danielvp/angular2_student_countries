import {Injectable} from '@angular/core';
import { Http, Headers, RequestOptions, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { User, MinimalUser } from './../models/user';
import { AuthError, REST } from './utills';
import 'rxjs';
import {JwtHelper} from 'angular2-jwt';


@Injectable()
export class AuthService{
    isLoggedIn : boolean = false;
    loggedInUser: MinimalUser;
    private authenticate: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    authenticateState$ : Observable<boolean> = this.authenticate.asObservable();
    redirectUrl: string = '/home';
    jwtHelper: JwtHelper = new JwtHelper();
    constructor(public http: Http){
         
        let token = localStorage.getItem('token');
        if(token){
            this.authenticate.next(true);
            this.isLoggedIn = true;
            this.loggedInUser = JSON.parse(localStorage.getItem('user'));
        }
    }

    signUp(user : User): Observable<any>{
        return this.http.post(`${REST.AUTH}/signup`,
            {
                user
            },
            this.getJsonOptions())
        .map(this.extractData)
        .catch(this.handleError);
    }
    signIn(user: User): Observable<any>{

        return this.http.post(`${REST.AUTH}/signin`,
            {
                user
            },
            this.getJsonOptions())

        .map(res=> {
            this.notifySubjects(res.json());
            return res;})
        .map(res => { this.storeLoginToken(res);
        return res; })
        .map(this.extractData)
        .catch(this.handleError);
    }

    getJsonOptions(): RequestOptions{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    storeLoginToken(res: Response): any{
        let body = res ? res.json() : {};
        if(res && body.token){
            localStorage.setItem('token', body.token);
            localStorage.setItem('user', JSON.stringify({email: body.email, firstName: body.firstName, lastName: body.lastName, country: body.country, university: body.university}));
            return {success: body.success, token: body.token}; 
        }
        return {success: body.success, message: body.msg}; 
    }

    private extractData(res: Response): AuthError {
        let body = res ? res.json() : {};
        return {success: body.success, message: body.msg}; 
    }
    notifySubjects(user: MinimalUser){
        if(user['success'] === true){
                   this.isLoggedIn = true;
            this.loggedInUser = user;
            this.authenticate.next(true);
        }else
        {
            this.authenticate.next(false);
        }
    }
    logout() {
        this.authenticate.next(false);
        this.isLoggedIn = false;
        this.loggedInUser = undefined;
        localStorage.removeItem('token');
    }
    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

    isAuthenticate() : boolean{
        return localStorage.getItem('token') ? true : false;
    }

}
