import {Injectable, OnInit } from '@angular/core';
import {URLSearchParams, Http, Headers, RequestOptions, Response, RequestOptionsArgs} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Subject } from 'rxjs/Subject';
import { User, MinimalUser } from './../models/user';
import { AuthError, REST } from './utills';
import 'rxjs';
import {JwtHelper} from 'angular2-jwt';

@Injectable()
export class UniversitiesService{
    isLoggedIn : boolean = false;
    loggedInUser: MinimalUser;
    private authenticate: Subject<boolean> ;
    authenticateState$ : Observable<boolean>;
    redirectUrl: string = '/home';
    jwtHelper: JwtHelper = new JwtHelper();
    constructor(public http: Http){
        this.authenticate = new Subject<boolean>();
        this.authenticateState$ = this.authenticate.asObservable();
    }
    getCountryCodes(): Observable<any>{
        return this.http.get(`${REST.UNIVERSITY}/country_codes`,
            this.getJsonOptions())
        .map(this.extractData)
        .catch(this.handleError);
    }

    getJsonOptions(): RequestOptions{
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    private extractData(res: Response): AuthError {
        let body = res.json();
        return body.body; 
    }

    searchUniversities(searchedTerm : string, countries: boolean = false){
        let requestOptions =  this.getJsonOptions(); 
        requestOptions.body = {'name': searchedTerm};
        var search = new URLSearchParams()
        if(searchedTerm){
            search.set('name', searchedTerm ? searchedTerm : '');
        }
        search.set('limit', '10');
        search.set('format', 'json');
        if(!countries)
        {
            return this.http.get(`${REST.UNIVERSITY}/search`,{search} )
                .map(request => 
                    {
                        return request.json().map((obj)=>obj.name);
                    })
                    .catch(this.handleError);
        }else
        {
            return this.http.get(`${REST.UNIVERSITY}/search`,{search} )
                .map(request => 
                    {
                        return request.json();
                    })
                    .catch(this.handleError);

        }
    }

    notifySubjects(user: MinimalUser){
        this.authenticate.next(true);
        this.isLoggedIn = true;
        this.loggedInUser = user;
    }
    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }
}
